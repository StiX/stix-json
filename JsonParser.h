
#pragma once

#include <cstdlib>
#include <cassert>
#include <cstring>
#include <climits>


#define DOUBLE_NAN_MASK		0x7FF0000000000000ULL
#define DOUBLE_PAYLOAD_MASK	0x000FFFFFFFFFFFFFULL
#define DOUBLE_TYPE_MASK	0x000E000000000000ULL
#define DOUBLE_VALUE_MASK	0x0001FFFFFFFFFFFFULL
#define DOUBLE_TYPE_OFFSET	49

#define JSON_STACK_DEPTH	1024

#ifdef _MSC_VER
#	define strcasecmp _stricmp
#endif


class JsonNode;

typedef unsigned long long	u64;

enum JsonResult
{
	eJsonSuccess,
	eJsonUnexpectedChar,
	eJsonBrokenValueKeyword,
	eJsonNonmatchingClosingBrace,
};

enum JsonValueType
{
	eJsonNumber,
	eJsonObject,
	eJsonArray,
	eJsonString,
	eJsonBool,
	eJsonNull,
	eDummy1,
	eDummy2,
	eJsonValueMax
};


class JsonValue
{
public:
					JsonValue();
	explicit		JsonValue(const double& val);
	explicit		JsonValue(JsonValueType type);
					JsonValue(JsonValueType type, void* data);

	JsonValueType	GetType() const;

	char*			AsString() const;
	double			AsNumber() const;
	bool			AsBool() const;
	bool			IsNull() const;

private:
	union
	{
		double		m_fval;
		u64			m_ival;
	};

	template <class T>
	T				GetValue() const;
};





//-------------------------------------------------------------------------------------------
inline JsonValue::JsonValue()
{
}

//-------------------------------------------------------------------------------------------
inline JsonValue::JsonValue(const double& val)
	: m_fval(val)
{
}

//-------------------------------------------------------------------------------------------
inline JsonValue::JsonValue(JsonValueType type)
{
	assert(type < eJsonValueMax);

	m_ival = DOUBLE_NAN_MASK | ((u64)type << DOUBLE_TYPE_OFFSET);
}

//-------------------------------------------------------------------------------------------
inline JsonValue::JsonValue(JsonValueType type, void* data)
{
	const u64 kVal = (u64)data;
	assert(kVal <= DOUBLE_PAYLOAD_MASK);
	assert(type < eJsonValueMax);

	m_ival = DOUBLE_NAN_MASK | ((u64)type << DOUBLE_TYPE_OFFSET) | kVal;
}

//-------------------------------------------------------------------------------------------
inline JsonValueType JsonValue::GetType() const
{
	return ((m_ival & DOUBLE_NAN_MASK) == DOUBLE_NAN_MASK) ? (JsonValueType)((m_ival & DOUBLE_TYPE_MASK) >> DOUBLE_TYPE_OFFSET) : eJsonNumber;
}

//-------------------------------------------------------------------------------------------
inline char* JsonValue::AsString() const
{
	assert(GetType() == eJsonString);
	return GetValue<char*>();
}

//-------------------------------------------------------------------------------------------
inline double JsonValue::AsNumber() const
{
	assert(GetType() == eJsonNumber);
	return m_fval;
}

//-------------------------------------------------------------------------------------------
template <>
inline bool JsonValue::GetValue<bool>() const
{
	return (m_ival & DOUBLE_VALUE_MASK) != 0;
}

//-------------------------------------------------------------------------------------------
inline bool JsonValue::AsBool() const
{
	assert(GetType() == eJsonBool);
	return GetValue<bool>();
}

//-------------------------------------------------------------------------------------------
inline bool JsonValue::IsNull() const
{
	assert(GetType() == eJsonNull);
	return GetValue<bool>();
}

//-------------------------------------------------------------------------------------------
template <class T>
inline T JsonValue::GetValue() const
{
	return (T)(m_ival & DOUBLE_VALUE_MASK);
}





class JsonNode
{
public:
						JsonNode();
						JsonNode(const JsonValue& value, JsonNode* pParentNode);

	bool				IsKey() const;
	const JsonValue&	GetValue() const;

	const JsonNode&		FindNode(const char* key) const;				// if you'll attempt to call this on array or another
	const JsonNode&		FindNodeCaseInsensitive(const char* key) const;	// non-object node, it will return reference to self

	size_t				GetElementsCount() const;
	const JsonNode&		operator[](size_t index) const;

	const JsonNode&		Next() const;

private:
	friend class		JsonParser;

	JsonValue			m_value;
	const JsonNode*		m_pNextNode;
	const JsonNode*		m_pNeighbourNode;
	JsonNode*			m_pParentNode;
	size_t				m_childsCount;

	struct StringCmp
	{
		static int		Cmp(const char* str1, const char* str2)
		{
			return strcmp(str1, str2);
		}
	};

	struct StringCaseInsensitiveCmp
	{
		static int		Cmp(const char* str1, const char* str2)
		{
			return strcasecmp(str1, str2);
		}
	};

						JsonNode(const JsonNode&);
	JsonNode&			operator=(const JsonNode&);

	template <class Cmp>
	const JsonNode&		InternalGetValue(const char* str) const;
};





//-------------------------------------------------------------------------------------------
inline JsonNode::JsonNode()
	: m_pNextNode(0), m_pNeighbourNode(0), m_childsCount(0)
{
}

//-------------------------------------------------------------------------------------------
inline JsonNode::JsonNode(const JsonValue& value, JsonNode* pParentNode)
	: m_value(value), m_pNextNode(0), m_pNeighbourNode(0), m_pParentNode(pParentNode), m_childsCount(0)
{
}

//-------------------------------------------------------------------------------------------
inline bool JsonNode::IsKey() const
{
	return m_childsCount >> (CHAR_BIT * sizeof(size_t) - 1) != 0;
}

//-------------------------------------------------------------------------------------------
inline const JsonValue& JsonNode::GetValue() const
{
	return m_value;
}

//-------------------------------------------------------------------------------------------
inline const JsonNode& JsonNode::FindNode(const char* key) const
{
	return InternalGetValue<StringCmp>(key);
}

//-------------------------------------------------------------------------------------------
inline const JsonNode& JsonNode::FindNodeCaseInsensitive(const char* key) const
{
	return InternalGetValue<StringCaseInsensitiveCmp>(key);
}

//-------------------------------------------------------------------------------------------
inline size_t JsonNode::GetElementsCount() const
{
	assert(GetValue().GetType() == eJsonObject || GetValue().GetType() == eJsonArray);

	return m_childsCount;
}

//-------------------------------------------------------------------------------------------
inline const JsonNode& JsonNode::operator[](size_t index) const
{
	assert(index < GetElementsCount());

	const JsonNode* pNode = m_pNeighbourNode;

	for (size_t i = 0; i < index; ++i)
	{
		pNode = pNode->m_pNextNode;
	}

	assert(pNode);
	return *pNode;
}

//-------------------------------------------------------------------------------------------
inline const JsonNode& JsonNode::Next() const
{
	return *m_pNeighbourNode;
}

//-------------------------------------------------------------------------------------------
template <class Cmp>
inline const JsonNode& JsonNode::InternalGetValue(const char* str) const
{
	assert(GetValue().GetType() == eJsonObject);

	const JsonNode* pNode = m_pNeighbourNode;

	const size_t kElementsCount = GetElementsCount();

	for (size_t i = 0; i < kElementsCount; ++i)
	{
		if (!Cmp::Cmp(str, pNode->GetValue().AsString()))
		{
			return *pNode;
		}

		pNode = pNode->m_pNextNode;
	}

	return *this;
}





class JsonParser
{
public:
	typedef void*		(*Alloc)(size_t);
	typedef void		(*Free)(void*);

						JsonParser();
						~JsonParser();

	const JsonNode&		GetRoot() const;
	JsonResult			ParseStringInplace(char* jsonString, size_t strLen);

	void				SetAllocator(const Alloc& memAlloc, const Free& memFree);

private:
	struct AllocationBlock
	{
		AllocationBlock* m_pNext;
		size_t			m_currentOffset;
		size_t			m_blockSize;
		size_t			m_padding;
		char			m_data[0];

	private:
						AllocationBlock(const AllocationBlock&);
		AllocationBlock& operator=(const AllocationBlock&);
	};

	struct JsonAllocator
	{
		Alloc			m_alloc;
		Free			m_free;
	};

	AllocationBlock*	m_pFirstBlock;
	AllocationBlock*	m_pCurrentBlock;

	JsonAllocator		m_allocator;

						JsonParser(const JsonParser&);
	JsonParser&			operator=(const JsonParser&);

	void				AllocateBlock(const char* currentJsonStrPos, const char* lastBlockStrPos, size_t& charsLeft);
	AllocationBlock*	CreateAllocationBlock(size_t memSize) const;
	void				FreeAllocationBlock(AllocationBlock* pBlock) const;
};





//-------------------------------------------------------------------------------------------
inline JsonParser::JsonParser()
	: m_pFirstBlock(0), m_pCurrentBlock(0)
{
	m_allocator.m_alloc = malloc;
	m_allocator.m_free = free;
}

//-------------------------------------------------------------------------------------------
inline JsonParser::~JsonParser()
{
	if (m_pFirstBlock)
	{
		FreeAllocationBlock(m_pFirstBlock);
	}
}

//-------------------------------------------------------------------------------------------
inline const JsonNode& JsonParser::GetRoot() const
{
	assert(m_pFirstBlock);

	return *(JsonNode*)m_pFirstBlock->m_data;
}

//-------------------------------------------------------------------------------------------
inline void JsonParser::SetAllocator(const Alloc& memAlloc, const Free& memFree)
{
	m_allocator.m_alloc = memAlloc;
	m_allocator.m_free = memFree;
}