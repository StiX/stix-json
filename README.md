Overview
--------------
**StiX Json** is extremely fast desctructive [json] parser with nice C++ API.

**StiX Json** pros:

* very fast;
* it is lightweight - only 2 files, totally around 700 lines of code;
* it allocates memory in huge blocks (ideal for memory pages) using provided allocator;
* it tries to predict memory requirements to minimize the amount of allocations;
* it is open source under MIT license.

Configuration
--------------
**StiX Json** uses stack to store information about parent and neighbour nodes. The size of stack can be configured with JSON_STACK_DEPTH define.

Performance
--------------
**StiX Json** was designed with performance and simplicity in mind. For best performance, you should compile *StiX Json* sources with **/Os (Favor small code)** VS flag. This gave me **32%** faster code.
My old performance test showed that **StiX Json** is roughly two times faster than **rapidjson**. I hope I'll provide a new test soon.